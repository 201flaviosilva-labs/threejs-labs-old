import Assets, { assetsPath } from "../../../Assets.js";

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 45;
const aspect = WIDTH / HEIGHT;
const near = 0.1;
const far = 2000;
const position = { x: 0, y: 600, z: 100 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.set(position.x, position.y, position.z);
// Responsivo
window.addEventListener("resize", () => {
	renderer.setSize(WIDTH, HEIGHT);
	camera.aspect = WIDTH / HEIGHT;
	camera.updateProjectionMatrix();
});

// Controller
const controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.target.set(0, 0, 0);

// Objetos

// Load
loadGLTF();
function loadGLTF() {
	const gltf = new THREE.GLTFLoader();

	gltf.load(Assets._3D.Blender.ToThreeJS.gltf, (gltf) => {
		const Mesh = gltf.scene;
		const scale = 30;
		Mesh.scale.set(scale, scale, scale);
		scene.add(Mesh);
		Mesh.position.x = 0;
		Mesh.position.y = 10;
		Mesh.position.z = 15;
	});
}

// Luz
// AmbientLight
const luzAmbiente = new THREE.AmbientLight(0xFFFFFF, 1);
scene.add(luzAmbiente);

// PointLight
const luzPoint = new THREE.PointLight(0xFFFFFF, 1);
luzPoint.position.set(0, 50, 0);
const luzHelper = new THREE.PointLightHelper(luzPoint);
scene.add(luzPoint);
scene.add(luzHelper);

// Update
const clock = new THREE.Clock();
update();
function update() {
	// objs.forEach(({ mixer }) => { mixer.update(clock.getDelta()); });
	renderer.render(scene, camera);
	requestAnimationFrame(update);
}
