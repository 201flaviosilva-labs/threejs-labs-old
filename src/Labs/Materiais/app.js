const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#cccccc");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 1000;
const position = { x: 0, y: 0, z: 20 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.z = position.z;

// Objetos
const objects = [];

const geometry = new THREE.SphereGeometry(3, 10, 10);

// Na criação
const material1 = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, flatShading: true }); // Mais leve Não é afetado pela luz
const material2 = new THREE.MeshLambertMaterial({ color: 'red', flatShading: true });
const material3 = new THREE.MeshPhongMaterial({ color: '#F00', flatShading: true });
// Materiais relacionados a fisica (os mais pesados de todos)
const material4 = new THREE.MeshStandardMaterial({ color: 'rgb(255,0,0)', flatShading: true });
const material5 = new THREE.MeshPhysicalMaterial({ color: 'hsl(0,100%,50%)', flatShading: true }); // mais pesado

// Editar pr´ós criação
material1.color.set("blue");
material2.color.set("#f0f0f0");
material3.color.setHSL(0, 1, .5);  // red
material4.color.set(0x00FFFF);
material5.color.set(0xFF00FF);
// material5.color.setRGB(105, 50, 0); // rgb está meio estrado

material3.flatShading = false; // Material Liso ou mais redondinho

let positionEsfera = -15;
const esfera1 = new THREE.Mesh(geometry, material1);
const esfera2 = new THREE.Mesh(geometry, material2);
const esfera3 = new THREE.Mesh(geometry, material3);
const esfera4 = new THREE.Mesh(geometry, material4);
const esfera5 = new THREE.Mesh(geometry, material5);
objects.push(esfera1);
objects.push(esfera2);
objects.push(esfera3);
objects.push(esfera4);
objects.push(esfera5);
objects.map(obj => {
	obj.position.x = positionEsfera;
	scene.add(obj);
	positionEsfera += 7;
});

// Luz
const color = 0xFFFFFF;
const intensity = 0.2;
const light = new THREE.DirectionalLight(color, intensity);
light.position.set(0, 0, position.z);
scene.add(light);

let rotation = 0;
animate();
function animate(time) {
	rotation += 0.01;
	const canvas = renderer.domElement;
	camera.aspect = canvas.clientWidth / canvas.clientHeight;
	camera.updateProjectionMatrix();

	objects.map(obj => {
		obj.rotation.set(rotation, rotation, rotation);
	});

	requestAnimationFrame(animate);
	renderer.render(scene, camera);
}
