import { GUI } from 'https://threejsfundamentals.org/threejs/../3rdparty/dat.gui.module.js';
import { ColorGUIHelper } from "./ColorGUIHelper.js";

// https://threejsfundamentals.org/threejs/lessons/threejs-lights.html

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;
const assets = "../../../Assets/";

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 45;
const aspect = 2;  // o padrão da tela
const near = 0.1;
const far = 100;
const position = { x: 0, y: 10, z: 20 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.set(position.x, position.y, position.z);

// Controller
const controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.target.set(0, 5, 0);
controls.update();

// Objetos
const planeSize = 40;

const loader = new THREE.TextureLoader();
const texture = loader.load('https://threejsfundamentals.org/threejs/resources/images/checker.png');
texture.wrapS = THREE.RepeatWrapping;
texture.wrapT = THREE.RepeatWrapping;
texture.magFilter = THREE.NearestFilter;
const repeats = planeSize / 2;
texture.repeat.set(repeats, repeats);

// Chão
const planeGeo = new THREE.PlaneBufferGeometry(planeSize, planeSize);
const planeMat = new THREE.MeshPhongMaterial({ map: texture, side: THREE.DoubleSide });
const mesh1 = new THREE.Mesh(planeGeo, planeMat);
mesh1.rotation.x = Math.PI * -.5;
scene.add(mesh1);

// Cubo
const cubeSize = 4;
const cubeGeo = new THREE.BoxBufferGeometry(cubeSize, cubeSize, cubeSize);
const cubeMat = new THREE.MeshPhongMaterial({ color: '#8AC' });
const mesh2 = new THREE.Mesh(cubeGeo, cubeMat);
mesh2.position.set(cubeSize + 1, cubeSize / 2, 0);
scene.add(mesh2);

// Esfera
const sphereRadius = 3;
const sphereWidthDivisions = 32;
const sphereHeightDivisions = 16;
const sphereGeo = new THREE.SphereGeometry(sphereRadius, sphereWidthDivisions, sphereHeightDivisions);
const sphereMat = new THREE.MeshPhongMaterial({ color: '#CA8' });
const mesh3 = new THREE.Mesh(sphereGeo, sphereMat);
mesh3.position.set(-sphereRadius - 1, sphereRadius + 2, 0);
scene.add(mesh3);

// Luz
// AmbientLight
// const color = 0xFFFFFF;
// const intensity = 1;
// const light = new THREE.AmbientLight(color, intensity); // Ilumida tudo

// HemisphereLight
// const colorSky = 0xB1E1FF;
// const colorFloor = 0xB97A20;
// const intensity = 1;
// const light = new THREE.HemisphereLight(colorSky, colorFloor, intensity);

// DirectionalLight
// const color = 0xFFFFFF;
// const intensity = 1;
// const light = new THREE.DirectionalLight(color, intensity);
// light.position.set(0, 10, 0);
// light.target.position.set(0, 0, 0);
// const helper = new THREE.DirectionalLightHelper(light);

// PointLight
// const color = 0xFFFFFF;
// const intensity = 1;
// const light = new THREE.PointLight(color, intensity);
// light.position.set(0, 10, 0);
// const helper = new THREE.PointLightHelper(light);

// SpotLight
const color = 0xFFFFFF;
const intensity = 1;
const light = new THREE.SpotLight(color, intensity);
light.position.set(0, 10, 0);
const helper = new THREE.SpotLightHelper(light);

// RectAreaLight // Apenas funciona com MeshStandardMateriale MeshPhysicalMaterial // https://threejs.org/docs/#api/en/lights/RectAreaLight
// const color = 0xFFFFFF;
// const width = 10;
// const height = 10;
// const intensity = 1;
// const light = new THREE.RectAreaLight(color, intensity, width, height);
// light.position.set(5, 10, 0);
// light.lookAt(0, -1, 0);

// const helper = new THREE.RectAreaLightHelper(light);


scene.add(light);
// scene.add(light.target);
// scene.add(helper);



// GUI Data
guiData();
function guiData() {
	function makeXYZGUI(gui, vector3, name, onChangeFn) {
		const folder = gui.addFolder(name);
		folder.add(vector3, 'x', -10, 10).onChange(onChangeFn);
		folder.add(vector3, 'y', 0, 10).onChange(onChangeFn);
		folder.add(vector3, 'z', -10, 10).onChange(onChangeFn);
		folder.open();
	}

	function updateLight() {
		// light.target.updateMatrixWorld();
		helper.update();
	}
	// updateLight();

	const gui = new GUI();
	// gui.addColor(new ColorGUIHelper(light, 'color'), 'value').name('color');

	// gui.addColor(new ColorGUIHelper(light, 'color'), 'value').name('skyColor');
	// gui.addColor(new ColorGUIHelper(light, 'groundColor'), 'value').name('groundColor');

	gui.addColor(new ColorGUIHelper(light, 'color'), 'value').name('color');
	// gui.add(light.target.position, 'x', -10, 10);
	// gui.add(light.target.position, 'z', -10, 10);
	// gui.add(light.target.position, 'y', -10, 10);

	gui.add(light, 'intensity', 0, 2, 0.01);
	gui.add(light, 'distance', 0, 40).onChange(updateLight);

	gui.add(light, 'penumbra', 0, 1, 0.01);

	makeXYZGUI(gui, light.position, 'position', updateLight);
	// makeXYZGUI(gui, light.target.position, 'target', updateLight);
}


// Update
animate();
function animate() {
	requestAnimationFrame(animate);
	renderer.render(scene, camera);
}
