import Stats from "../../../Lib/Stats.js";

let tecla = null;
export default class Game {
	constructor() {
		this.init();
		this.create();
		this.update();
	}

	init() {
		this.WIDTH = window.innerWidth - 10;
		this.HEIGHT = window.innerHeight - 10;

		this.assets = "../../Assets/";
		this.loader = new THREE.TextureLoader();
	}

	create() {
		// Scene
		this.scene = new THREE.Scene();
		this.status();
		this.render();
		this.camera();
		this.responsive();
		this.controller();
		this.objects();
		this.light();
	}

	status() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		document.body.appendChild(this.stats.dom);
	}

	render() {
		// Render
		this.renderer = new THREE.WebGLRenderer({ antialias: true });
		this.renderer.setClearColor("#f0f0f0");
		this.renderer.setSize(this.WIDTH, this.HEIGHT);
		document.body.appendChild(this.renderer.domElement);
	}

	controller() {
		// Controller
		this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
		this.controls.target.set(0, 0, 0);
		this.controls.update();
	}

	camera() {
		// Camera
		const fov = 45;
		const aspect = this.WIDTH / this.HEIGHT;
		const near = 0.1;
		const far = 1000;
		const position = { x: 0, y: 50, z: 100 };
		this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
		this.camera.position.set(position.x, position.y, position.z);
	}

	responsive() {
		// Responsivo
		window.addEventListener("resize", () => {
			this.renderer.setSize(this.WIDTH, this.HEIGHT);
			this.camera.aspect = this.WIDTH / this.HEIGHT;
			this.camera.updateProjectionMatrix();
		});
	}

	objects() {
		// Objetos
		this.createFloor();
		this.creatPlayer();
		this.changeDirection();
	}

	createFloor() {
		// Textura Chão
		const floor = this.loader.load(this.assets + "Texturas/Floor/StoneFloorDiffuse.jpg");

		const planeSize = 100;

		floor.wrapS = THREE.RepeatWrapping;
		floor.wrapT = THREE.RepeatWrapping;
		floor.magFilter = THREE.NearestFilter;
		const repeats = planeSize / 2;
		floor.repeat.set(repeats, repeats);

		// Chão
		const planeGeo = new THREE.PlaneBufferGeometry(planeSize, planeSize);
		const planeMat = new THREE.MeshPhongMaterial({ map: floor, side: THREE.DoubleSide });
		const floorMesh = new THREE.Mesh(planeGeo, planeMat);
		floorMesh.rotation.x = Math.PI * -.5;
		this.scene.add(floorMesh);
	}

	creatPlayer() {
		// Player
		const geometryPlayer = new THREE.BoxGeometry(2, 2, 4);
		const materialPlayer = new THREE.MeshPhongMaterial({ color: 0xff0000 });
		this.player = new THREE.Mesh(geometryPlayer, materialPlayer);
		this.player.position.set(0, 1, 0);
		this.scene.add(this.player);
		this.changeDirection();
	}

	changeDirection() {
		// Obter a tecla selecionada
		document.addEventListener("keydown", teclaDown, false);
		function teclaDown(event) {
			const key = event.code;
			tecla = key;
		};

		document.addEventListener("keyup", teclaUp, false);
		function teclaUp() {
			tecla = null;
		};
	}

	light() {
		// Luz
		this.AmbientLight();
		this.PointLight();
	}

	AmbientLight() {
		// AmbientLight
		const luzAmbiente = new THREE.AmbientLight(0xFFFFFF, 1);
		this.scene.add(luzAmbiente);
	}

	PointLight() {
		// PointLight
		const luzPoint = new THREE.PointLight(0xFFFFFF, 0.5);
		luzPoint.position.set(0, 50, 0);
		const luzHelper = new THREE.PointLightHelper(luzPoint);
		this.scene.add(luzPoint);
		this.scene.add(luzHelper);
	}

	update() {
		// Update
		this.stats.begin();
		this.renderer.render(this.scene, this.camera);
		this.movePlayer();
		this.controls.update();
		this.stats.end();
		requestAnimationFrame(this.update.bind(this));
	}

	movePlayer() {
		const speed = 1;
		const maxPosition = 50 - 4;

		if ((tecla === "KeyD" || tecla === "ArrowRight") && this.player.position.x <= maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.rotateY(22.5 * Math.PI);
			this.player.position.x += speed;

		} else if ((tecla === "KeyA" || tecla === "ArrowLeft") && this.player.position.x >= -maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.rotateY(67.5 * Math.PI);
			this.player.position.x -= speed;

		} else if ((tecla === "KeyS" || tecla === "ArrowDown") && this.player.position.z <= maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.position.z += speed;

		} else if ((tecla === "KeyW" || tecla === "ArrowUp") && this.player.position.z >= -maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.rotateY(45 * Math.PI);
			this.player.position.z -= speed;

		} else if (tecla === "Space") {// Reset
			this.player.rotation.set(0, 0, 0);
			this.player.position.set(0, 1, 0);
		}
	}
}
