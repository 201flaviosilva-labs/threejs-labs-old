import Stats from "../../../Lib/Stats.js";
import { randomNumber, randomColor, randomColorRGB, randomColorRGBA } from "../../../util.js";

export default class Game {
	constructor() {
		this.init();
		this.create();
		this.update();
	}

	init() {
		this.WIDTH = window.innerWidth - 10;
		this.HEIGHT = window.innerHeight - 10;

		this.playerSize = 2;

		this.loader = new THREE.TextureLoader();
	}

	create() {
		// Scene
		this.scene = new THREE.Scene();
		this.status();
		this.render();
		this.camera();
		this.controller();
		this.responsive();
		this.creatPlayer();
		this.AmbientLight();
	}

	status() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		document.body.appendChild(this.stats.dom);
	}

	render() {
		// Render
		this.renderer = new THREE.WebGLRenderer({ antialias: true, });
		this.renderer.setClearColor(0x000000);
		this.renderer.setSize(this.WIDTH, this.HEIGHT);
		document.body.appendChild(this.renderer.domElement);
	}

	controller() {
		// Controller
		this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
		this.controls.target.set(0, 0, 0);
		this.controls.update();
	}

	camera() {
		// Camera
		const fov = 45;
		const aspect = this.WIDTH / this.HEIGHT;
		const near = 0.1;
		const far = 10000;
		const position = { x: 0, y: 50, z: 100 };
		this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
		this.camera.position.set(position.x, position.y, position.z);
	}

	responsive() {
		// Responsivo
		window.addEventListener("resize", () => {
			this.renderer.setSize(this.WIDTH, this.HEIGHT);
			this.camera.aspect = this.WIDTH / this.HEIGHT;
			this.camera.updateProjectionMatrix();
		});
	}

	creatPlayer() {
		// Player
		const geometryPlayer = new THREE.BoxGeometry(this.playerSize, this.playerSize, this.playerSize);
		const materialPlayer = new THREE.MeshPhongMaterial({ color: 0xff0000, });
		this.player = new THREE.Mesh(geometryPlayer, materialPlayer);
		this.player.position.set(0, 0, 0);
		this.scene.add(this.player);
	}

	createPlayerPath(position) {
		const { x, y, z } = position;
		const geometryPlayer = new THREE.BoxGeometry(this.playerSize, this.playerSize, this.playerSize);
		const materialPlayer = new THREE.MeshPhongMaterial({ color: 0x0f000, opacity: 0.25, transparent: true, });
		const playerPath = new THREE.Mesh(geometryPlayer, materialPlayer);
		playerPath.position.set(x, y, z);
		this.scene.add(playerPath);
	}

	AmbientLight() {
		// AmbientLight
		const luzAmbiente = new THREE.AmbientLight(0xFFFFFF, 1);
		this.scene.add(luzAmbiente);
	}

	update() {
		// Update
		this.stats.begin();
		this.renderer.render(this.scene, this.camera);
		this.movePlayer();
		this.controls.update();
		this.stats.end();
		requestAnimationFrame(this.update.bind(this));
	}

	movePlayer() {
		const speed = 1;
		const KeyBoardKey = randomNumber(0, 5);

		this.createPlayerPath(this.player.position);

		switch (KeyBoardKey) {
			case 0: {
				this.player.position.x += speed;
				break;
			}
			case 1: {
				this.player.position.x -= speed;
				break;
			}
			case 2: {
				this.player.position.y += speed;
				break;
			}
			case 3: {
				this.player.position.y -= speed;
				break;
			}
			case 4: {
				this.player.position.z += speed;
				break;
			}
			case 5: {
				this.player.position.z -= speed;
				break;
			}
		}
	}
}
